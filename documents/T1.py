u = ("toto", 12, 1.32, "Tourcoing")

for x in u:
    print(x)

print("toto" in u)
print(3 in u)

print(u[0])
#u[0] = "titi" ne marche pas

u = (1, 1, 1, 1, 1)
print(u)

u = ("toto", 12, 1.32, "Tourcoing")
nom, age, taille, ville = u
print(ville)

moyenne = {"toto": 14.5, "titi": 14.5, "tutu": 9.5}
#ou
moyenne = {} #divtionnaire vide
moyenne["toto"] = 14.5
moyenne["titi"] = 14.5
moyenne["tutu"] = 9.5

carre = {i:i**2 for i in range(1, 6)}

print (moyenne["toto"])
moyenne["toto"] = 13
print(moyenne["toto"])

for eleve in moyenne.keys(): #parcour les cles
    print(eleve)
for eleve in moyenne: #parcours par cles, notation plus simple
    print(eleve)

for note in moyenne.values(): #parcours par valeurs
    print(note)

for eleve in moyenne:
    print(eleve, ":", moyenne[eleve])

print("toto" in moyenne)
print(14.5 in moyenne)

print("toto" in moyenne.values())
print(14.5 in moyenne.values())

moyenne["toto"] = 13 #on modifie la moyenne de toto
moyenne["tata"] = 12 #on ajout un eleve tata avec la moyenne 12
print(moyenne["tata"])

del moyenne["toto"] #on retire toto et sa note du dico

moyenne = {} #les dicos autorisent les doublons de valeurs mais pas d'indice
moyenne["toto"] = 14.5
moyenne["toto"] = 13.5 #toto sera donc remplacé
moyenne["titi"] = 14.5
moyenne["tutu"] = 9.5
moyenne["tata"] = 9.5
print(moyenne)


fermerFichier = input("appuyer sur 'enter' pour fermer le fichier")

