#Exercice 150

# 3

#Exercice 151

# tableau de 10 entrée avec la valeur recherchée qui n'est pas dans le tableau

#Exercice 152

def recherche_dichotomique(t, v):
    g = 0
    d = len(t) - 1
    nbFois = 0
    while g <= d:
        nbFois += 1
        m = (g + d) // 2
        if t[m] < v:
            g = m + 1
        elif t[m] > v:
            d = m - 1
        else:
            return nbFois
    return nbFois

#exercice 153

def nb_de_tours(n):
    t = [0] * n
    return recherche_dichotomique(t, 1)

print(nb_de_tours(100))


#exercice 154

#Il faut, dans le pire cas, 7 tours





