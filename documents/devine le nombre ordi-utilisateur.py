print("Choisissez un nombre entre 1 et 100")

def moitie_champ_recherche(min, max):
    return (min + max) // 2

trouve = False

min = 1
max = 100

nbEssais = 0

while not trouve:
    moitie = moitie_champ_recherche(min, max)

    print(moitie)

    reponse = input("'plus grand', 'plus petit', 'bravo' : ")

    if reponse == "plus grand":
        min = moitie + 1
    elif reponse == "plus petit":
        max = moitie - 1
    else:
        print(nbEssais)
        break

    nbEssais += 1


