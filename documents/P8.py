from turtle import Turtle
from random import randint

turtle = Turtle()

#exercice 125 

def randomTab():
    g = [[randint(1, 9999) for j in range (30)] for i in range (30)]
    return g

tab = randomTab()


#exercice 126

def plusGrand(g):
    plusGrand = 0
    for i in range (len(g)):
        for j in range (len(g[i])):
            valeurG = g[i][j]

            if valeurG > plusGrand:
                plusGrand = valeurG

    return plusGrand

print("plus grand", plusGrand(tab))


#exercice 127

def PGDPP(g): #plus grand des plus petits
    plusGrand = 0
    for i in range (len(g)):
        minimum = min(g[i])
        if minimum > plusGrand:
            plusGrand = minimum
    return plusGrand


#exercice 129
g = [[True, False, True], [False, True, False], [True, False, True]]

def dessine(g):
    turtle.hideturtle()
    for i in range (len(g)):
        for j in range (len(g[i])):
            turtle.speed('fastest')
            if g[i][j]:
                turtle.penup()
                turtle.setx(j * 100)
                turtle.sety(i * -100)
                turtle.pendown()
                turtle.begin_fill()
                turtle.forward(100)
                turtle.left(90)
                turtle.forward(100)
                turtle.left(90)
                turtle.forward(100)
                turtle.left(90)
                turtle.forward(100)
                turtle.end_fill()
                turtle.left(90)

#dessine(g)