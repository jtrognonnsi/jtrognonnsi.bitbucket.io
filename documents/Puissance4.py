from random import randint


c = 7
l = 6

j1 = 1
j2 = 2

def grille_vide():
    return [[0] * 7 for i in range (6)]



def affiche(g):
    print("| ", end="")
    for i in range (c): #affiche une ligne avec le numéro de chaque colonne
        print(i, end=' | ')
    print("\r")
    for i in range (c):
        print("____", end='')
    print("_ \n")

    for i in range (l): #affiche la grille
        print("| ", end="")
        for j in range (c):
            if g[(l - 1) - i][j] == 0: #affiche un point si la case est vide
                print('.', end = ' | ')
            elif g[(l - 1) - i][j] == 1:
                print('X', end = ' | ')
            else:
                print('O', end = ' | ')
        print("\r")
        for j in range (c):
            print("----", end='')
        print("- \r")



grille = grille_vide()



def coup_possible(g, c):
    if (c < 0 or c >= 7):
        return False
    return g[l - 1][c] == 0




def jouer(g, j, c):
    i = 0
    while g[i][c] != 0:
        i += 1
    g[i][c] = j




def horiz(g, j, l, c):
    compteur = 1
    i = 1
    while compteur != 4 and c + i < len(g[i]): #regarde si il y a une ligne de 4 à droite
        #print(l, c, i)
        if g[l][c + i] == j:
            compteur += 1
        else:
            break
        i += 1
    
    if compteur == 4: #regarde si le compteur est à 4
        return True

    i = 1

    while compteur != 4 and c - i >= 0: #regare si il y a une ligne de 4 à gauche
        if g[l][c - i] == j:
            compteur += 1
        else:
            break
        i += 1

    if compteur == 4:
        return True
    else:
        return False




def vert(g, j, l, c):
    compteur = 1
    i = 1
    while compteur != 4 and l + i < len(g): #regarde si il y a une colonne de 4 en haut
        if g[l + i][c] == j:
            compteur += 1
        else:
            break
        i += 1
    if compteur == 4:
        return True
    
    i = 1

    while compteur != 4 and l - i >= 0: #regarde si il y a une colonne de 4 en bas
        if g[l - i][c] == j:
            compteur += 1
        else:
            break
        i += 1
    
    if compteur == 4:
        return True
    else:
        return False



def diag(g, j, l, c):
    compteur = 1
    i = 1

    while compteur != 4 and l + i < len(g) and c + i < len(g[i]): #regarde la diagonale haut droite
        if g[l + i][c + i] == j:
            compteur += 1
        else:
            break
        i += 1
    if compteur == 4:
        return True
    
    i = 1

    while compteur != 4 and l - i >= 0 and c - i >= 0: #regarde la diagonale bas gauche
        if g[l - i][c - i] == j:
            compteur += 1
        else:
            break
        i += 1
    if compteur == 4:
        return True

    #------------------------------------------------------------------------ (séparation pour y voir plus clair)

    i = 1
    compteur = 1



    while compteur != 4 and l + i < len(g) and c - i >= 0: #regarde la diagonale haut gauche
        if g[l + i][c - i] == j:
            compteur += 1
        else:
            break
        i += 1
    if compteur == 4:
        return True
    
    i = 1

    while compteur != 4 and l - i >= 0 and c + i < len(g[i]): #regarde la diagonale bas gauche
        if g[l - i][c + i] == j:
            compteur += 1
        else:
            break
        i += 1
    if compteur == 4:
        return True
    else:
        return False




def victoire(g, joueur):
    for i in range (l):
        for j in range (c):
            if g[i][j] == joueur:
                if horiz(g, joueur, i, j):
                    return True
                elif vert(g, joueur, i, j):
                    return True
                elif diag(g, joueur, i, j):
                    return True
    return False


def match_nul(g):
    for i in range (c):
        if g[l - 1 ][i] == 0:
            return False
    return True
    


def coup_aleatoire(g, j):
    possible = False
    while not possible: #change la valeur de colonne tant que le robot n'a pas joué
        colonne = randint(0, 6)
        if (coup_possible(g, colonne)):
            jouer(g, j, colonne)
            possible = True
        else:
            colonne = randint(0, 6)







typePartie = input("'robot-joueur' ou 'robot-robot' : ")
fin = False



if typePartie == "robot-joueur": #regarde le type de partie
    print("Vous êtes O")
    while not fin: #boucle tant que la partie n'est pas finie
        coup_aleatoire(grille, j1) #tour de l'ordinateur
        if (match_nul(grille)):
            print("match nul")
            break
        affiche(grille)

        if victoire(grille, j1):
            print("perdu(e)")
            break




        coup = int(input("colonne : ")) #tour du joueur

        possible = False
        while not possible:
            if(coup_possible(grille, coup)):
                jouer(grille, j2, coup)
                possible = True
            else:
                print("coup impossible")
                coup = int(input("colonne : "))

        if (match_nul(grille)):
            print("match nul")
            break
        
        affiche(grille)
        
        if victoire(grille, j2):
            print("gagne(e)")
            break


elif typePartie == "robot-robot": 
    while not fin:
        coup_aleatoire(grille, j1) #tour de j1
        if (match_nul(grille)):
            print("match nul")
            break
        affiche(grille)

        if victoire(grille, j1):
            print("j1 gagne")
            break



        coup_aleatoire(grille, j2) #tour de j2
        if (match_nul(grille)):
            print("match nul")
            break
        affiche(grille)

        if victoire(grille, j2):
            print("j2 gagne")
            break
else:
    print("type mal saisi")
