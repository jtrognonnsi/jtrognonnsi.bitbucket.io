nbChoisi = int(input("Choisissez un nombre entre 0 et 100 : "))

trouve = False

d = 0
f = 100

def moitie_champ_recherche(min, max):
    return (min + max) // 2

def inclu(min, max, nbChoisi):
    if nbChoisi < min:
        return False
    elif nbChoisi > max:
        return False
    else:
        return True

while not trouve:
    m = moitie_champ_recherche(d, f)

    if inclu(d, f, nbChoisi):
        if nbChoisi < m:
            d = nbChoisi + 1
            nbChoisi = int(input("Trop petit : "))
        elif nbChoisi > m:
            f = nbChoisi - 1
            nbChoisi = int(input("Trop grand : "))
        else:
            f = nbChoisi - 1
            nbChoisi = int(input("Trop grand : "))

    if f - d <= 1:
        trouve = True
        print("bravo")

